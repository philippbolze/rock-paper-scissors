package com.bolze.rockpaperscissors;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = { "job.autorun.enabled=false" })
public class RoundServiceTest {

	@Autowired
	private RoundService testee;

	@Test
	public void testCalculateRoundResult1() {
		// GIVEN
		Shape player1Shape = Shape.PAPER;
		Shape player2Shape = Shape.SCISSORS;

		// WHEN
		Result result = testee.calculateRoundResult(player1Shape, player2Shape);

		// THEN
		assertEquals(Result.PLAYER_2_WINS, result);
	}

	@Test
	public void testCalculateRoundResult2() {
		// GIVEN
		Shape player1Shape = Shape.SCISSORS;
		Shape player2Shape = Shape.PAPER;

		// WHEN
		Result result = testee.calculateRoundResult(player1Shape, player2Shape);

		// THEN
		assertEquals(Result.PLAYER_1_WINS, result);
	}

	@Test
	public void testCalculateRoundResult3() {
		// GIVEN
		Shape player1Shape = Shape.ROCK;
		Shape player2Shape = Shape.SCISSORS;

		// WHEN
		Result result = testee.calculateRoundResult(player1Shape, player2Shape);

		// THEN
		assertEquals(Result.PLAYER_1_WINS, result);
	}

	@Test
	public void testCalculateRoundResult4() {
		// GIVEN
		Shape player1Shape = Shape.SCISSORS;
		Shape player2Shape = Shape.ROCK;

		// WHEN
		Result result = testee.calculateRoundResult(player1Shape, player2Shape);

		// THEN
		assertEquals(Result.PLAYER_2_WINS, result);
	}

	@Test
	public void testCalculateRoundResultDraw() {
		// GIVEN
		Shape player1Shape = Shape.ROCK;
		Shape player2Shape = Shape.ROCK;

		// WHEN
		Result result = testee.calculateRoundResult(player1Shape, player2Shape);

		// THEN
		assertEquals(Result.DRAW, result);
	}

}
