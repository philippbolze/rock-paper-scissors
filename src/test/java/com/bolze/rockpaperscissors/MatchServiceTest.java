package com.bolze.rockpaperscissors;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bolze.rockpaperscissors.strategy.PaperStrategy;
import com.bolze.rockpaperscissors.strategy.RockStrategy;
import com.bolze.rockpaperscissors.strategy.Strategy;

@SpringBootTest(properties = { "job.autorun.enabled=false" })
public class MatchServiceTest {

	@Mock
	private RoundService roundService;

	@Autowired
	private MatchService testee;

	@Test
	public void testRunDraw() {
		// GIVEN
		int numberOfRounds = 2;
		Strategy player1Strategy = new RockStrategy();
		Strategy player2Strategy = new RockStrategy();

		// WHEN
		MatchResult result = testee.run(numberOfRounds, player1Strategy, player2Strategy);

		// THEN
		assertEquals(0, result.getWinsPlayer1());
		assertEquals(0, result.getWinsPlayer2());
		assertEquals(2, result.getNumberOfDraws());
		assertEquals(Result.DRAW, result.getResult());
	}

	@Test
	public void testRunPlayer2Wins() {
		// GIVEN
		int numberOfRounds = 2;
		Strategy player1Strategy = new RockStrategy();
		Strategy player2Strategy = new PaperStrategy();

		// WHEN
		MatchResult result = testee.run(numberOfRounds, player1Strategy, player2Strategy);

		// THEN
		assertEquals(0, result.getWinsPlayer1());
		assertEquals(2, result.getWinsPlayer2());
		assertEquals(0, result.getNumberOfDraws());
		assertEquals(Result.PLAYER_2_WINS, result.getResult());
		assertEquals(Result.PLAYER_2_WINS, result.getResult());
	}

}
