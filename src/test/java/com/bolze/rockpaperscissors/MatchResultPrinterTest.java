package com.bolze.rockpaperscissors;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = { "job.autorun.enabled=false" })
public class MatchResultPrinterTest {

	@Autowired
	private MatchResultPrinter matchResultPrinter;

	@Test
	public void testPrint() {
		// GIVEN
		MatchResult match = new MatchResult();
		match.setNumberOfDraws(1);
		match.setWinsPlayer1(2);
		match.setWinsPlayer2(0);
		RoundInfo round1 = new RoundInfo();
		round1.setPlayer1Shape(Shape.SCISSORS);
		round1.setPlayer2Shape(Shape.PAPER);
		round1.setResult(Result.PLAYER_1_WINS);
		round1.setRoundNumber(1);

		RoundInfo round2 = new RoundInfo();
		round2.setPlayer1Shape(Shape.PAPER);
		round2.setPlayer2Shape(Shape.PAPER);
		round2.setResult(Result.DRAW);
		round2.setRoundNumber(2);

		RoundInfo round3 = new RoundInfo();
		round3.setPlayer1Shape(Shape.PAPER);
		round3.setPlayer2Shape(Shape.ROCK);
		round3.setResult(Result.PLAYER_1_WINS);
		round3.setRoundNumber(3);

		List<RoundInfo> rounds = new ArrayList<RoundInfo>();
		rounds.add(round1);
		rounds.add(round2);
		rounds.add(round3);
		match.setRounds(rounds);
		match.setResult(Result.PLAYER_1_WINS);

		// WHEN
		String result = matchResultPrinter.print(match);
		System.out.println(result);

		// THEN
		String expected = "\r\n" + "         | player A | player B | Winner: \r\n"
				+ "---------|-------------------------------\r\n" + "round 1  | scissors   paper      player A\r\n"
				+ "round 2  | paper      paper      draw\r\n" + "round 3  | paper      rock       player A\r\n" + "\r\n"
				+ "Wins player A: 2\r\n" + "Wins player B: 0\r\n" + "Draws: 1\r\n" + "\r\n" + "Winner: player A";

		assertEquals(expected, result);
	}

}
