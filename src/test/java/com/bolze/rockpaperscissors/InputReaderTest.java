package com.bolze.rockpaperscissors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bolze.rockpaperscissors.strategy.RockStrategy;
import com.bolze.rockpaperscissors.strategy.Strategy;

@SpringBootTest(properties = { "job.autorun.enabled=false" })
public class InputReaderTest {

	@Autowired
	private InputReader testee;

	@Test
	public void testReadNumberOfRounds() {
		// GIVEN
		String input = "32";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		Scanner scanner = new Scanner(in);

		// WHEN
		int result = testee.readNumberOfRounds(scanner);

		// THEN
		assertEquals(32, result);
	}

	@Test
	public void testRNumberOfRoundsNotANumber() {
		// GIVEN
		String input = "abc";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		Scanner scanner = new Scanner(in);

		// WHEN
		boolean exceptionOccured = false;
		try {
			testee.readNumberOfRounds(scanner);
		} catch (Exception e) {
			exceptionOccured = true;
		}

		// THEN
		assertTrue(exceptionOccured);
	}

	@Test
	public void testRNumberOfRoundsNegativeNumber() {
		// GIVEN
		String input = "-4";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		Scanner scanner = new Scanner(in);

		// WHEN
		boolean exceptionOccured = false;
		try {
			testee.readNumberOfRounds(scanner);
		} catch (Exception e) {
			exceptionOccured = true;
		}

		// THEN
		assertTrue(exceptionOccured);
	}

	@Test
	public void testReadStrategy() {
		// GIVEN
		String input = "2";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		Scanner scanner = new Scanner(in);

		// WHEN
		Strategy result = testee.readStrategy(scanner, "A");

		// THEN
		assertEquals(RockStrategy.class, result.getClass());
	}

	@Test
	public void testReadStrategyInvalidInput() {
		// GIVEN
		String input = "x";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		Scanner scanner = new Scanner(in);

		// WHEN
		boolean exceptionOccured = false;
		try {
			testee.readNumberOfRounds(scanner);
		} catch (Exception e) {
			exceptionOccured = true;
		}

		// THEN
		assertTrue(exceptionOccured);
	}

}
