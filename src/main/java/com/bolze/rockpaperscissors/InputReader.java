package com.bolze.rockpaperscissors;

import java.util.Scanner;

import org.springframework.stereotype.Component;

import com.bolze.rockpaperscissors.strategy.PaperStrategy;
import com.bolze.rockpaperscissors.strategy.RandomStrategy;
import com.bolze.rockpaperscissors.strategy.RockStrategy;
import com.bolze.rockpaperscissors.strategy.Strategy;

@Component
public class InputReader {

	public Strategy readStrategy(Scanner scanner, String playerName) {
		System.out.println();
		System.out.println(Messages.ENTER_STRATEGY + playerName);
		System.out.println(Messages.STRATEGY_RANDOM_DESCRIPTION);
		System.out.println(Messages.STRATEGY_ROCK_DESCRIPTION);
		System.out.println(Messages.STRATEGY_PAPER_DESCRIPTION);
		String choice = scanner.next();
		switch (choice) {
		case Messages.STRATEGY_RANDOM_INPUT:
			return new RandomStrategy();
		case Messages.STRATEGY_ROCK_INPUT:
			return new RockStrategy();
		case Messages.STRATEGY_PAPER_INPUT:
			return new PaperStrategy();
		default:
			System.out.println(Messages.ERROR_NOT_VALID);
			return readStrategy(scanner, playerName);
		}
	}

	public int readNumberOfRounds(Scanner scanner) {
		System.out.println();
		System.out.println(Messages.ENTER_ROUNDS);
		String choice = scanner.next();
		try {
			int numberOfRounds = Integer.valueOf(choice);
			if (numberOfRounds < 1) {
				System.out.println(Messages.ERROR_NOT_A_POSITIVE_NUMBER);
				return readNumberOfRounds(scanner);
			}
			return numberOfRounds;
		} catch (NumberFormatException e) {
			System.out.println(Messages.ERROR_NOT_A_NUMBER);
			return readNumberOfRounds(scanner);
		}
	}

}
