package com.bolze.rockpaperscissors;

public enum Shape {

	ROCK(0),
	PAPER(1),
	SCISSORS(2);

	private int value;

	private Shape(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public String getDisplayName() {
		switch (this) {
		case ROCK:
			return Messages.ROCK;
		case PAPER:
			return Messages.PAPER;
		case SCISSORS:
			return Messages.SCISSORS;
		}
		return Messages.EMPTY;
	}

}
