package com.bolze.rockpaperscissors;

public class Messages {

	public static final String EMPTY = "";
	public static final String SPACE = " ";
	public static final Object NEW_LINE = "\r\n";

	public static final String ENTER_ROUNDS = "Enter number of rounds to play.";
	public static final String ENTER_STRATEGY = "Enter strategy for ";
	public static final String STRATEGY_RANDOM_DESCRIPTION = "1: random";
	public static final String STRATEGY_ROCK_DESCRIPTION = "2: always rock";
	public static final String STRATEGY_PAPER_DESCRIPTION = "3: always paper";
	public static final String STRATEGY_RANDOM_INPUT = "1";
	public static final String STRATEGY_ROCK_INPUT = "2";
	public static final String STRATEGY_PAPER_INPUT = "3";

	public static final String PLAYER_1_NAME = "player A";
	public static final String PLAYER_2_NAME = "player B";
	public static final String DRAW = "draw";

	public static final String ERROR_NOT_A_NUMBER = "ERROR:Your entry was not a number.";
	public static final String ERROR_NOT_VALID = "ERROR: Please enter \"1\" or \"2\".";
	public static final String ERROR_NOT_A_POSITIVE_NUMBER = "ERROR: Please enter a positive number greater than zero.";

	public static final String ROUND = "round ";
	public static final String WINS = "Wins ";
	public static final String DRAWS = "Draws: ";
	public static final String WINNER = "Winner: ";

	public static final String ROCK = "rock    ";
	public static final String PAPER = "paper   ";
	public static final String SCISSORS = "scissors";

}
