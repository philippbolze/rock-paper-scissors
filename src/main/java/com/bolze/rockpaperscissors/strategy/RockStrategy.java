package com.bolze.rockpaperscissors.strategy;

import com.bolze.rockpaperscissors.Shape;

public class RockStrategy implements Strategy {

	public Shape run() {
		return Shape.ROCK;
	}

}
