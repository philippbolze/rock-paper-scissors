package com.bolze.rockpaperscissors.strategy;

import java.util.Random;

import com.bolze.rockpaperscissors.Shape;

public class RandomStrategy implements Strategy {

	private static final Shape[] ALL_SHAPES = { Shape.ROCK, Shape.PAPER, Shape.SCISSORS };

	public Shape run() {
		Random random = new Random();
		return ALL_SHAPES[random.nextInt(3)];
	}

}
