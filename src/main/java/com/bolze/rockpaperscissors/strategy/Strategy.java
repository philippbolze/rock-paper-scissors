package com.bolze.rockpaperscissors.strategy;

import com.bolze.rockpaperscissors.Shape;

public interface Strategy {

	public Shape run();
}
