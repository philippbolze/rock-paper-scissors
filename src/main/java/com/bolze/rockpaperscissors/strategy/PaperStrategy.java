package com.bolze.rockpaperscissors.strategy;

import com.bolze.rockpaperscissors.Shape;

public class PaperStrategy implements Strategy {

	public Shape run() {
		return Shape.PAPER;
	}

}
