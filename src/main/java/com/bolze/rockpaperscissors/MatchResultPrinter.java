package com.bolze.rockpaperscissors;

import org.springframework.stereotype.Component;

@Component
public class MatchResultPrinter {

	public String print(MatchResult matchResult) {
		StringBuilder builder = new StringBuilder();
		buildTable(matchResult, builder);
		buildSummary(matchResult, builder);
		return builder.toString();
	}

	private void buildTable(MatchResult matchResult, StringBuilder builder) {
		builder.append(Messages.NEW_LINE);
		builder.append("         | ");
		builder.append(Messages.PLAYER_1_NAME + " | " + Messages.PLAYER_2_NAME + " | " + Messages.WINNER);
		builder.append(Messages.NEW_LINE);
		builder.append("---------|-------------------------------");
		builder.append(Messages.NEW_LINE);

		for (RoundInfo round : matchResult.getRounds()) {
			builder.append(Messages.ROUND + round.getRoundNumber());
			builder.append((round.getRoundNumber() < 10) ? "  | " : " | ");
			builder.append(round.getPlayer1Shape().getDisplayName());
			builder.append("   ");
			builder.append(round.getPlayer2Shape().getDisplayName());
			builder.append("   ");
			builder.append(round.getResult().getWinnerName());
			builder.append(Messages.NEW_LINE);
		}
	}

	private void buildSummary(MatchResult matchResult, StringBuilder builder) {
		builder.append(Messages.NEW_LINE);
		builder.append(Messages.WINS + Messages.PLAYER_1_NAME + ": " + matchResult.getWinsPlayer1());
		builder.append(Messages.NEW_LINE);
		builder.append(Messages.WINS + Messages.PLAYER_2_NAME + ": " + matchResult.getWinsPlayer2());
		builder.append(Messages.NEW_LINE);
		builder.append(Messages.DRAWS + matchResult.getNumberOfDraws());
		builder.append(Messages.NEW_LINE);
		builder.append(Messages.NEW_LINE);
		builder.append(Messages.WINNER + matchResult.getResult().getWinnerName());
	}

}
