package com.bolze.rockpaperscissors;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.bolze.rockpaperscissors.strategy.Strategy;

@SpringBootApplication
public class RockPaperScissorsApplication {

	@Autowired
	private InputReader inputReader;
	@Autowired
	private MatchService matchService;
	@Autowired
	private MatchResultPrinter matchResultPrinter;

	public static void main(String[] args) {
		SpringApplication.run(RockPaperScissorsApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(prefix = "job.autorun", name = "enabled", havingValue = "true", matchIfMissing = true)
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			Scanner scanner = new Scanner(System.in);
			int numberOfRounds = inputReader.readNumberOfRounds(scanner);
			Strategy player1Strategy = inputReader.readStrategy(scanner, Messages.PLAYER_1_NAME);
			Strategy player2Strategy = inputReader.readStrategy(scanner, Messages.PLAYER_2_NAME);
			MatchResult result = matchService.run(numberOfRounds, player1Strategy, player2Strategy);
			System.out.println(matchResultPrinter.print(result));
		};
	}

}
