package com.bolze.rockpaperscissors;

import org.springframework.stereotype.Component;

@Component
public class RoundService {

	public Result calculateRoundResult(Shape shapePlayer1, Shape shapePlayer2) {
		int valuePlayer1 = shapePlayer1.getValue();
		int valuePlayer2 = shapePlayer2.getValue();

		if (valuePlayer1 == valuePlayer2) {
			return Result.DRAW;
		}

		// Each shape loses against the player with the next higher value
		// 0 is considered higher than 2
		if (incrementShapeValue(valuePlayer2) == valuePlayer1) {
			return Result.PLAYER_1_WINS;
		}

		return Result.PLAYER_2_WINS;
	}

	private int incrementShapeValue(int value) {
		return (value == 2) ? 0 : ++value;
	}

}
