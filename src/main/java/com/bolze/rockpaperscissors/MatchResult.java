package com.bolze.rockpaperscissors;

import java.util.List;

public class MatchResult {

	private List<RoundInfo> rounds;
	private Result result;
	private int winsPlayer1;
	private int winsPlayer2;
	private int numberOfDraws;

	public List<RoundInfo> getRounds() {
		return rounds;
	}

	public void setRounds(List<RoundInfo> rounds) {
		this.rounds = rounds;
	}

	public int getWinsPlayer1() {
		return winsPlayer1;
	}

	public void setWinsPlayer1(int winsPlayer1) {
		this.winsPlayer1 = winsPlayer1;
	}

	public int getWinsPlayer2() {
		return winsPlayer2;
	}

	public void setWinsPlayer2(int winsPlayer2) {
		this.winsPlayer2 = winsPlayer2;
	}

	public int getNumberOfDraws() {
		return numberOfDraws;
	}

	public void setNumberOfDraws(int numberOfDraws) {
		this.numberOfDraws = numberOfDraws;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

}
