package com.bolze.rockpaperscissors;

public class RoundInfo {

	private Result result;
	private Shape player1Shape;
	private Shape player2Shape;
	private int roundNumber;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public Shape getPlayer1Shape() {
		return player1Shape;
	}

	public void setPlayer1Shape(Shape player1Shape) {
		this.player1Shape = player1Shape;
	}

	public Shape getPlayer2Shape() {
		return player2Shape;
	}

	public void setPlayer2Shape(Shape player2Shape) {
		this.player2Shape = player2Shape;
	}

	public int getRoundNumber() {
		return roundNumber;
	}

	public void setRoundNumber(int roundNumber) {
		this.roundNumber = roundNumber;
	}

}
