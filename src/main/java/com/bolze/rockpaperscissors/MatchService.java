package com.bolze.rockpaperscissors;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bolze.rockpaperscissors.strategy.Strategy;

@Component
public class MatchService {

	@Autowired
	private RoundService roundService;

	public MatchResult run(int rounds, Strategy strategyPlayer1, Strategy strategyPlayer2) {
		MatchResult matchResult = new MatchResult();
		List<RoundInfo> roundInfos = new ArrayList<RoundInfo>(rounds);

		int numberOfDraws = 0;
		int winsPlayer1 = 0;
		int winsPlayer2 = 0;

		for (int roundNumber = 1; roundNumber <= rounds; roundNumber++) {
			Shape shapePlayer1 = strategyPlayer1.run();
			Shape shapePlayer2 = strategyPlayer2.run();
			Result roundResult = roundService.calculateRoundResult(shapePlayer1, shapePlayer2);

			if (roundResult == Result.DRAW) {
				numberOfDraws++;
			}

			if (roundResult == Result.PLAYER_1_WINS) {
				winsPlayer1++;
			}

			if (roundResult == Result.PLAYER_2_WINS) {
				winsPlayer2++;
			}

			RoundInfo roundInfo = createRoundInfo(shapePlayer1, shapePlayer2, roundResult, roundNumber);
			roundInfos.add(roundInfo);
		}

		matchResult.setRounds(roundInfos);
		matchResult.setNumberOfDraws(numberOfDraws);
		matchResult.setWinsPlayer1(winsPlayer1);
		matchResult.setWinsPlayer2(winsPlayer2);
		matchResult.setResult(getMatchResult(winsPlayer1, winsPlayer2));

		return matchResult;
	}

	private Result getMatchResult(int winsPlayer1, int winsPlayer2) {
		if (winsPlayer1 == winsPlayer2) {
			return Result.DRAW;
		} else if (winsPlayer1 > winsPlayer2) {
			return Result.PLAYER_1_WINS;
		} else {
			return Result.PLAYER_2_WINS;
		}
	}

	private RoundInfo createRoundInfo(Shape shapePlayer1, Shape shapePlayer2, Result result, int roundNumber) {
		RoundInfo round = new RoundInfo();
		round.setPlayer1Shape(shapePlayer1);
		round.setPlayer2Shape(shapePlayer2);
		round.setResult(result);
		round.setRoundNumber(roundNumber);
		return round;
	}

}
