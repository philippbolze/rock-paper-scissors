package com.bolze.rockpaperscissors;

public enum Result {
	DRAW,
	PLAYER_1_WINS,
	PLAYER_2_WINS;

	public String getWinnerName() {
		switch (this) {
		case DRAW:
			return Messages.DRAW;
		case PLAYER_1_WINS:
			return Messages.PLAYER_1_NAME;
		case PLAYER_2_WINS:
			return Messages.PLAYER_2_NAME;
		}
		return Messages.EMPTY;
	}
}